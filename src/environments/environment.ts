// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
 /* firebaseConfig: {
    apiKey: "AIzaSyBwH0GDpWqxX7_LDsu9CyXo4MKP_ilcSFg",
    authDomain: "loginfirebase-78368.firebaseapp.com",
    databaseURL: "https://loginfirebase-78368.firebaseio.com",
    projectId: "loginfirebase-78368",
    storageBucket: "loginfirebase-78368.appspot.com",
    messagingSenderId: "7881118777",
    appId: "1:7881118777:web:24d2299ba0d4db5dd969e8",
    measurementId: "G-JMTSZ6FCKK"
  }*/
  firebaseConfig:{
    
    apiKey: "AIzaSyBP7FapRbW9yi7kKEZfWvJUMlG-0rzVQh4",
    authDomain: "lpmg11.firebaseapp.com",
    databaseURL: "https://lpmg11.firebaseio.com",
    projectId: "lpmg11",
    storageBucket: "lpmg11.appspot.com",
    messagingSenderId: "48122795248",
    appId: "1:48122795248:web:4f6484382a7d74e5489e35",
    measurementId: "G-46BJME6BJT"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
