import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import { DatafirebaseService } from '../../services/datafirebase.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  validations_form: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';

  validation_messages= {
    'email':[
      { type: 'required', message: 'es requerido un Correo'},
      { type: 'pattern', message: 'Introdusca un correo electronic Valido'}
    ],
    'password': [
      { type: 'required', message: 'es requerido un password'},
      { type: 'minlengh', message: 'la contraseña debe tener almenos 5 caracters de logitud'}
    ]
  };
  constructor(
    private nav: NavController,
    private loadingController: LoadingController,
    private datafirefase: DatafirebaseService,
    private formBuilder: FormBuilder
  ) { } 

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }

  tryRegister(value){
    this.datafirefase.registerUser(value)
     .then(res => {
       console.log(res);
       this.errorMessage = "";
       this.successMessage = "Your account has been created. Please log in.";
     }, err => {
       console.log(err);
       this.errorMessage = err.message;
       this.successMessage = "";
     })
     this.saveTodo();
  }
  
  async saveTodo() {
    const loading = await this.loadingController.create({
      message: "registering...",
      duration: 2000
    });
    await loading.present();
  }

  goLoginPage(){
    this.nav.navigateBack('');
  }
}
