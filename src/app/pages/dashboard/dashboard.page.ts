import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, MenuController } from '@ionic/angular';
import { DatafirebaseService } from '../../services/datafirebase.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  userEmail: string;
 
  constructor(
    private navCtrl: NavController,
    private authService: DatafirebaseService,
    private menu: MenuController
    ) { }
  ngOnInit() {
    if(this.authService.userDetails()){
      this.userEmail = this.authService.userDetails().email;
    }else{
      this.navCtrl.navigateBack('');
    }
  }
  

}
