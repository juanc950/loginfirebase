import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, MenuController } from '@ionic/angular';
import { DatafirebaseService } from '../../services/datafirebase.service';
@Component({
  selector: 'app-setup',
  templateUrl: './setup.page.html',
  styleUrls: ['./setup.page.scss'],
})
export class SetupPage implements OnInit {

  constructor(
    private navCtrl: NavController,
    private authService: DatafirebaseService,
    private menu: MenuController
  ) { }

  ngOnInit() {
    if(this.authService.userDetails()){
      
    }else{
      this.navCtrl.navigateBack('');
    }
  }
  desconectarse(){
    this.authService.logoutUser()
    .then(res => {
      console.log(res);
      this.navCtrl.navigateBack('');
    })
    .catch(error => {
      console.log(error);
    })
  }

}
