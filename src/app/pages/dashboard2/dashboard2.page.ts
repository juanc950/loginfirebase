import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, MenuController } from '@ionic/angular';
import { DatafirebaseService } from '../../services/datafirebase.service';
import { FCM } from '@ionic-native/fcm/ngx';
import { Todo, TodoService } from './../../services/todo.service'

@Component({
  selector: 'app-dashboard2',
  templateUrl: './dashboard2.page.html',
  styleUrls: ['./dashboard2.page.scss'],
})
export class Dashboard2Page implements OnInit {
  userEmail: string;
  todos: Todo[];

  constructor(
    private navCtrl: NavController,
    private authService: DatafirebaseService,
    private menu: MenuController,
    private todoService: TodoService,
    private fcm: FCM
  ) { }

  ngOnInit() {
    if(this.authService.userDetails()){
      this.userEmail = this.authService.userDetails().email;

      this.todoService.getTodos().subscribe(res => {
        this.todos = res;
        console.log(this.todos);
      });
      this.fcm.getToken().then(token => {
        console.log(token)
      })
      
    }else{
      this.navCtrl.navigateBack('');
    }    
  }

  remove(item) {
    this.todoService.removeTodo(item.id);
  }

}
