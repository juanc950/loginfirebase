import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/menu/login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MenuPage,
    children: [
      { path: 'login', loadChildren: () => import('../login/login.module').then(m => m.LoginPageModule) },
      { path: 'register', loadChildren: () => import('../register/register.module').then(m => m.RegisterPageModule) },
      { path: 'dashboard', loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardPageModule) },
      { path: 'dashboard2', loadChildren: () => import('../dashboard2/dashboard2.module').then(m => m.Dashboard2PageModule) },
      { path: 'setup', loadChildren: () => import('../setup/setup.module').then( m => m.SetupPageModule)},
      { path: 'details', loadChildren: () => import('../todo-details/todo-details.module').then( m => m.TodoDetailsPageModule)},
      { path: 'details/:id', loadChildren: () => import('../todo-details/todo-details.module').then( m => m.TodoDetailsPageModule)},
    
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule { }
