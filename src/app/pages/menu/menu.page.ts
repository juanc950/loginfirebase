import { Component, OnInit } from '@angular/core';
import { DatafirebaseService } from '../../services/datafirebase.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  

  constructor(
    private  authService: DatafirebaseService
  ) { }

  ngOnInit() {
  }

  pages = [
    {
      title: 'MENU',
      url: '/menu/login',
      icon: 'home'
    },
    {
      title: 'Menu Sistem',
      subdata: [
        {
          title: 'Welcom Ionic',
          url: '/menu/dashboard',
          icon: 'logo-ionic'
        },
        {
          title: 'Ionic FireTodos',
          url: '/menu/dashboard2',
          icon: 'jet',
        },
        {
          title: 'Config',
          url: '/menu/setup',
          icon: 'build',
        }
      ]
    }
  ];

  
}
