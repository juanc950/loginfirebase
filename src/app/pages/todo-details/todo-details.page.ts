import { Component, OnInit } from '@angular/core';
import { Todo, TodoService } from './../../services/todo.service'
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
import { DatafirebaseService } from './../../services/datafirebase.service';

@Component({
  selector: 'app-todo-details',
  templateUrl: './todo-details.page.html',
  styleUrls: ['./todo-details.page.scss'],
})
export class TodoDetailsPage implements OnInit {
  userEmail: string;

  todo: Todo = {
    task: "",
    description: "",
    createdAt: new Date().getTime(),
    priority: 0
  };

  todoId = null;
  constructor(
    private authService: DatafirebaseService,
    private route: ActivatedRoute,
    private nav: NavController,
    private todoService: TodoService,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
    if (this.authService.userDetails()) {
      this.userEmail = this.authService.userDetails().email;

      this.todoId = this.route.snapshot.params["id"];
      if (this.todoId) {
        this.loadTodo();
      }

    } else {
      this.nav.navigateBack('');
    }


  }

  async loadTodo() {
    const loading = await this.loadingController.create({
      message: "Loading Todo.."
    });
    await loading.present();

    this.todoService.getTodo(this.todoId).subscribe(res => {
      loading.dismiss();
      this.todo = res;
    });
  }

  async saveTodo() {
    const loading = await this.loadingController.create({
      message: "Saving Todo.."
    });
    await loading.present();

    if (this.todoId) {
      this.todoService.updateTodo(this.todo, this.todoId).then(() => {
        loading.dismiss();
        //this.nav.navigateBack("/home");
      });
    } else {
      this.todoService.addTodo(this.todo).then(() => {
        loading.dismiss();
        //this.nav.navigateBack("/home");
      });
    }
  }

}
