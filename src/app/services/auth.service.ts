import { Injectable } from '@angular/core';
import { AngularFireAuth } from "angularfire2/auth";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private AFA : AngularFireAuth) { }

  Login(email:string,password:string){
    this.AFA.auth.signInWithEmailAndPassword(email,password).then(res =>{
      console.log("Todo bien: "+res)
    }).catch(err =>{
      console.log("Ocurrio Algo: "+err)
    })
  }
}